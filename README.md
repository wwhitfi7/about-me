# About Me

Welcome to my GitLab! Since you're here, feel free to check out projects I've done here.
Additionally, don't hesitate to [visit my github account as well!](https://www.github.com/wwhitfi7)
Why do I have both? The simple answer is that I needed both, the more complex answer is that some of my schoolwork was completed on my github account while some of it had to be completed here, like my senior project with the KSU EV Team!